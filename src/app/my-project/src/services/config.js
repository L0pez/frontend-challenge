// General configuration for Nestoria service
const configService = {
  apiUrl: 'https://api.nestoria.co.uk'
}

export default configService
