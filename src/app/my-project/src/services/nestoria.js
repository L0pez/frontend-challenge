import trae from 'trae'
import configService from './config'

// Create the Trae instance for Nestoria service
const nestoriaService = trae.create({
  baseUrl: configService.apiUrl
})

export default nestoriaService
