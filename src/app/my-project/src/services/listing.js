import nestoriaService from './nestoria'
const listingService = {}

// Method to get all listings from Nestoria service
listingService.get = async function () {
  const params = {
    encoding: 'json',
    pretty: 1,
    action: 'search_listings',
    country: 'uk',
    listing_type: 'buy',
    place_name: 'brighton',
    number_of_results: 10
  }
  const response = await nestoriaService.get('/api', {params})
  return response.data
}

export default listingService
