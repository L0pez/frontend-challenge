# frontend-challenge

> ### Reto de Frontend, desarrollado con VueJS y VuetifyJS.

He elegido VueJS porque es el framework de Javascript para desarrollo front-end que más me agrada, además este tiene algunas ventajas frente a otros, en temas de rendimiento y curva de aprendizaje. También he utilizado la libreria Vuetify como un complemento a Vue, que nos provee componentes hechos en material design listos para utilizarse.

### 
## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
